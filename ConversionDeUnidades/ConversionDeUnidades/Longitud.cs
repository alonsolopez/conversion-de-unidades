﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1LOC Base
//*************************
// Bloque de Código fuente:
// Clase Longitud
// Breve: incluye todos los métodos de de conversiones
//de entre el sistema métrico y del sistema imperial, de 
//unidades de LONGITUD
//*************************
//TODO: terminarlo
//UNDONE
//HACK
namespace ConversionDeUnidades
{
    //1LOC Base
    public class Longitud
    {
        /// <summary>
        /// Constructor para hacer conversiones de longitud
        /// entre sistema métrico e imperial.
        /// </summary>
        public Longitud()
        {

        }
        /// <summary>
        /// Me permite obtener las PULGADAS que corresponden a los 
        /// MILIMETROS que ingresamos en el parámetro.
        /// </summary>
        /// <param name="mm">Los milimetros que deseo convertir en 
        /// pulgadas</param>
        /// <returns>Las PULGADAS que corresponden a los milimetros 
        /// ingresados</returns>
        public decimal convMmAPlg(double mm)
        {
            //return decimal.Parse(mm * 0.03937).ToString());
            return decimal.Parse(mm.ToString()) /25.4m;
        }
        /// <summary>
        /// Nos va a permitir obtener Los centimetros a una
        /// conversión a pulgadas
        /// </summary>
        /// <param name="cm">Los centrimetros se convertirán en
        /// pulgadas</param>
        /// <returns>Nos devuelve las pulgadas de los 
        /// centrimetros que hemos introducido</returns>
        public decimal convCmAPlg(double cm)
        {
            //return decimal.Parse(cm * 0.3937).ToString());
            return decimal.Parse(cm+"") /2.54m;
        }
        /// <summary>
        /// Me permite obtener los MILIMETROS que le corresponderán a 
        /// las pulgadas que queremos obtener en en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS que deseo convertir
        /// a MILIMETROS</param>
        /// <returns>Nos devolverá los MILIMETROS que le corresponden
        /// en pulgadas ingresados por el usuario</returns>
        public decimal convPlgAMm(double plg)
        {
            return decimal.Parse(plg.ToString()) / 0.03937m;
        }
        /// <summary>
        /// Me permite obtener los CENTIMETROS que le corresponderán a 
        /// las PULGADAS que queremos obtener en en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS que deseo convertir
        /// a CENTIMETROS</param>
        /// <returns>Nos devolverá los CENTIMETROS que le corresponden
        /// en PULGADAS ingresados por el usuario</returns>
        public decimal convPlgACm(double plg)
        {
            return decimal.Parse(plg.ToString()) / 0.39370m;
        }

        /// <summary>
        /// Me permite obtener los CENTIMETROS que le corresponderán a 
        /// los MILIMETROS que queremos obtener en en la entrada por el usuario.
        /// </summary>
        /// <param name="mm">Los MILIMETROS que deseo convertir
        /// a CENTIMETROS</param>
        /// <returns>Nos devolverá los CENTIMETROS que le corresponden
        /// en MILIMETROS ingresados por el usuario</returns>
        public decimal convMmACm(double mm)
        {
            return decimal.Parse(mm.ToString()) / 10.000m;
        }
        /// <summary>
        /// Me va a permitir obtener los MILIMETROS que le corresponderán a los
        /// CENTIMETROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="cm">Los CENTIMETROS que deseo convertir
        /// a MILIMETROS</param>
        /// <returns>Nos devolverá los MILIMETROS que le corresponden
        /// en CENTIMETROS ingresados por el usuario</returns>
        public decimal convCmAMm(double cm)
        {
            return decimal.Parse(cm.ToString()) / 0.10000m;
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS que le corresponderán a los
        /// METROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS que deseo convertir
        /// a PULGADAS</param>
        /// <returns>Nos devolverá las PULGADAS que le corresponden
        /// en METROS ingresados por el usuario</returns>
        public decimal convMtAPlg(double mt)
        {
            return decimal.Parse(mt.ToString()) / 39.370m;
        }


        //-----------------------------------------------------
        /// <summary>
        /// Permite obtener las Yardas que corresponden a los
        /// metros que ingresamos con el parametro
        /// </summary>
        /// <param name="Mt"> los metros que se convertiran a yardas</param>
        /// <returns>Las yardas correspondientes a la conversion</returns>

        public decimal convMtAYd(double Mt)
        {
            return decimal.Parse(Mt.ToString()) * 100m;
        }
        //-----------------------------------------------------
        /// <summary>
        /// Permite obtener las Yardas que corresponden a los
        /// kilometros que ingresamos con el parametro
        /// </summary>
        /// <param name="Km"> los kilometros que se convertiran a yardas</param>
        /// <returns>Las yardas correspondientes a la conversion</returns>

        public decimal convKmAYd(double Km)
        {
            return decimal.Parse(Km.ToString()) * 1093.61m;
        }

        //-----------------------------------------------------
            /// <summary>
            /// Permite obtener los pies que corresponden a las
            /// pulgadas que ingresamos con el parametro
            /// </summary>
            /// <param name="ft"> los pies que se convertiran a pulgadas</param>
            /// <returns>Las pulgadas correspondientes a la conversion</returns>

            public decimal convFtAPlg(double ft)
            {
                return decimal.Parse(ft.ToString()) * 12.00m;
            }
            //-----------------------------------------------------
            /// <summary>
            /// Permite obtener los pies que corresponden a las
            /// yardas que ingresamos con el parametro
            /// </summary>
            /// <param name="yd"> las yardas que se convertiran a pies</param>
            /// <returns>Los pies correspondientes a la conversion</returns>

            public decimal convYdAFt(double ft)
            {
                return decimal.Parse(ft.ToString()) * 3.00m;
            }


            /// <summary>
            /// Me va a permitir obtener los METROS que le corresponderán a las
            /// PULGADAS que queremos obtener en la entrada por el usuario.
            /// </summary>
            /// <param name="plg">Las PULGADAS que deseo convertir
            /// a METROS</param>
            /// <returns>Nos devolverá los METROS que le corresponden
            /// en PULGADAS ingresados por el usuario</returns>
            public decimal convPlgAMt(double plg)
        {
            return decimal.Parse(plg.ToString()) * 39.370m;
        }
        /// <summary>
        /// Me va a permitir obtener los MILIMETROS que le corresponderán a los
        /// METROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS que deseo convertir
        /// a MILIMETROS</param>
        /// <returns>Nos devolverá los MILIMETROS que le corresponden
        /// en METROS ingresados por el usuario</returns>
        public decimal convMtAMm(double mt)
        {
            return decimal.Parse(mt.ToString()) / 0.0010000m;
        }
        /// <summary>
        /// Me va a permitir obtener los CENTIMETROS que le corresponderán a los
        /// METROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS que deseo convertir
        /// a CENTIMETROS</param>
        /// <returns>Nos devolverá los CENTIMETROS que le corresponden
        /// en METROS ingresados por el usuario</returns>
        public decimal convMtACm(double mt)
        {
            return decimal.Parse(mt.ToString()) * 100m;
        }
        /// <summary>
        /// Me va a permitir obtener los METROS que le corresponderán a los
        /// CENTIMETROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="cm">Los CENTIMETROS que deseo convertir
        /// a METROS</param>
        /// <returns>Nos devolverá los METROS que le corresponden
        /// en CENTIMETROS ingresados por el usuario</returns>
        public decimal convCmAMt(double cm)
        {
            return decimal.Parse(cm.ToString()) / 100.00m;
        }

        /// <summary>
        /// Este métdodo método permite convertir el equivalente de los pies que reciben a Pulgadas
        /// </summary>
        /// <param name="pie">La cantidad en PIE que queremos convertir a pulgadas</param>
        /// <returns>Las pulgadas que son equivalente a los PIEs ingresados</returns>
        public decimal convPieAPlg(double pie)
        {
            return decimal.Parse((pie*12)+"");
        }



        
        /// <summary>
        /// Este métdodo método permite convertir el equivalente de los pies que reciben a Yardas
        /// </summary>
        /// <param name="pie">La cantidad en PIE que queremos convertir a Yardas</param>
        /// <returns>Las Yardas que son equivalente a los PIEs ingresados</returns>
        public decimal convPieAYd(double pie)
        {
            return decimal.Parse((pie / 3) + "");
        }

        //convPieAMilla
        /// <summary>
        /// Este métdodo método permite convertir el equivalente de los pies que reciben a Millas
        /// </summary>
        /// <param name="pie">La cantidad en PIE que queremos convertir a Millas</param>
        /// <returns>Las Millas que son equivalente a los PIEs ingresados</returns>
        public decimal convPieAMilla(double pie)
        {
            return decimal.Parse(pie.ToString()) * 3m*1760m;
        }

        //convPieAMillaNaut
        /// <summary>
        /// Este métdodo método permite convertir el equivalente de los pies que reciben a Millas Náuticas
        /// </summary>
        /// <param name="pie">La cantidad en PIE que queremos convertir a Millas Náuticas</param>
        /// <returns>Las Millas Náuticas que son equivalente a los PIEs ingresados</returns>
        public decimal convPieAMillaNaut(double pie)
        {
            return decimal.Parse(pie+ "") * 3m * 2025.4m;
        }

        //convYdAPlg
        /// <summary>
        /// Este métdodo método permite convertir el equivalente de las yardas que reciben a Pulgadas
        /// </summary>
        /// <param name="yd">La cantidad en Yarda que queremos convertir a Pulgadas</param>
        /// <returns>Las Pulgadas que son equivalente a las Yardas ingresadas</returns>
        public decimal convYdAPlg(double yd)
        {
            return (decimal.Parse(yd+ "" )/ 3m) /12m;
        }

        //convYdAMilla
        /// <summary>
        /// Este métdodo método permite convertir el equivalente de los Yarda que reciben a Millas 
        /// </summary>
        /// <param name="yd">La cantidad en Yarda que queremos convertir a Millas </param>
        /// <returns>Las Millas Náuticas que son equivalente a los Yarda ingresados</returns>
        public decimal convYdAMilla(double yd)
        {
            return decimal.Parse(yd+ "") / 1760m;
        }


        //convYdAMillaNaut
        //convYdAMilla
        /// <summary>
        /// Este métdodo método permite convertir el equivalente de los yarda que reciben a Millas Náuticas
        /// </summary>
        /// <param name="yd">La cantidad en Yarda que queremos convertir a Millas Náuticas</param>
        /// <returns>Las Millas Náuticas que son equivalente a los Yarda ingresados</returns>
        public decimal convYdAMillaNaut(double yd)
        {
            return decimal.Parse(yd+ "") / 2025.4m ;
        }

        //convMillaAPlg
        /// <summary>
        /// Este métdodo método permite convertir el equivalente de las Millas Náuticas que reciben a Pulgadas
        /// </summary>
        /// <param name="millas">La cantidad en Millas que queremos convertir a Millas Náuticas</param>
        /// <returns>Las Millas Náuticas que son equivalente a los Yarda ingresados</returns>
        public decimal convMillaAPlg(double millas)
        {
            return ((decimal.Parse(millas+ "") / 1760m)/3m)/12m ;
        }

        
        //convMillaAPlg
        /// <summary>
        /// Este métdodo método permite convertir el equivalente de las Millas Náuticas que reciben a Pie
        /// </summary>
        /// <param name="millas">La cantidad en Millas que queremos convertir a Millas Náuticas</param>
        /// <returns>Las Millas Náuticas que son equivalente a los Yarda ingresados</returns>
        public decimal convMillaAPie(double millas)
        {
            return ((decimal.Parse(millas+ "") / 1760m) / 3m) / 12m) ;
        }
        //convMillaAYd

        //convMillaAYdNaut
        //convMillaNautAPlg
        //convMillaNautAPie
        //convMillaNautAYd
        //convMillaNautAMilla
    }
}
