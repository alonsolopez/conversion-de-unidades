﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1LOC Base
//*************************
// Bloque de Código fuente:
// Clase Area
// Breve: incluye todos los métodos de de conversiones
//de entre el sistema métrico y del sistema imperial, de 
//unidades de AREA
//*************************
//TODO: terminarlo
//UNDONE
//HACK
namespace ConversionDeUnidades
{
    public class Area
    {
        /// <summary>
        /// Constructor para hacer conversiones de Area
        /// entre sistema métrico e imperial.
        /// </summary>
        public Area()
        {

        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUADRADAS que le corresponderán a los
        /// CENTIMETROS CUADRADOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="cm">Los CENTIMETROS CUADRADOS que deseo convertir
        /// a PULGADAS CUADRADAS</param>
        /// <returns>Nos devolverá las PULGADAS CUADRADAS que le corresponden
        /// en CENTIMETROS CUADRADOS ingresados por el usuario</returns>
        public decimal convCmCuadAPlgCuad(double cm)
        {
            return decimal.Parse(cm.ToString()) * 0.1550m).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los CENTIMETROS CUADRADAS que le corresponderán a las
        /// PULGADAS CUADRADOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUADRADAS que deseo convertir
        /// a CENTIMETROS CUADRADAS</param>
        /// <returns>Nos devolverá los CENTIMETROS CUADRADAS que le corresponden
        /// en PULGADAS CUADRADAS ingresados por el usuario</returns>
        public decimal convPlgCuadACmCuad(double plg)
        {
            return decimal.Parse(plg.ToString()) * 6.4516m;
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUADRADAS que le corresponderán a los
        /// METROS CUADRADOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS CUADRADOS que deseo convertir
        /// a PULGADAS CUADRADAS</param>
        /// <returns>Nos devolverá las PULGADAS CUADRADAS que le corresponden
        /// en METROS CUADRADOS ingresados por el usuario</returns>
        public decimal convMtCuadAPlgCuad(double mt)
        {
            return decimal.Parse(mt.ToString()) * 1550m;
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUADRADAS que le corresponderán a las
        /// HECTAREAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="hect">Las HECTAREAS que deseo convertir
        /// a PULGADAS CUADRADAS</param>
        /// <returns>Nos devolverá las PULGADAS CUADRADAS que le corresponden
        /// en HECTAREAS ingresados por el usuario</returns>
        public decimal convHectAPLgCuad(double hect)
        {
            return decimal.Parse(hect.ToString()) * 15500031m;
        }
        /// <summary>
        /// Me va a permitir obtener las HECTAREAS que le corresponderán a las
        /// PULGADAS CUADRADAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUADRADAS que deseo convertir
        /// a HECTAREAS</param>
        /// <returns>Nos devolverá las HECTAREAS  que le corresponden
        /// en PULGADAS CUADRADAS ingresados por el usuario</returns>
        public decimal convPlgCuadAHect(double plg)
        {
            return decimal.Parse(plg.ToString()) * 0.000000064516m;
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUADRADAS que le corresponderán a los
        /// KILOMETROS CUADRADOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="km">Los KILOMETROS CUADRADOS que deseo convertir
        /// a PULGADAS CUADRADAS</param>
        /// <returns>Nos devolverá las PULGADAS CUADRADAS que le corresponden
        /// en KILOMETROS CUADRADOS ingresados por el usuario</returns>
        public decimal convKmCuadAPlgCuad(double km)
        {
            return decimal.Parse(km.ToString()) * 1.550000000m;
        }
        /// <summary>
        /// Me va a permitir obtener los KILOMETROS CUADRADOS que le corresponderán a las
        /// PULGADAS CUADRADAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUADRADAS que deseo convertir
        /// a KILOMETROS CUADRADOS</param>
        /// <returns>Nos devolverá los KILOMETROS CUADRADOS que le corresponden
        /// en PULGADAS CUADRADAS ingresados por el usuario</returns>
        public decimal convPlgCuadAKmCuad(double plg)
        {
            return decimal.Parse(plg.ToString()) * 0.00000000064516m;
        }
        /// <summary>
        /// Me va a permitir obtener los PIES CUADRADOS que le corresponderán a los
        /// CENTIMETROS CUADRADOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="cm">Los CENTIMETROS CUADRADOS que deseo convertir
        /// a PIES CUADRADOS</param>
        /// <returns>Nos devolverá los PIES CUADRADOS que le corresponden
        /// en CENTIMETROS CUADRADOS ingresados por el usuario</returns>
        public decimal convCmCuadAPieCuad(double cm)
        {
            return decimal.Parse(cm.ToString()) / 929m;
        }
        /// <summary>
        /// Me va a permitir obtener los CENTIMETROS CUADRADOS que le corresponderán a los
        /// PIES CUADRADOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="pie">Los PIES CUADRADOS que deseo convertir
        /// a CENTIMETROS CUADRADOS</param>
        /// <returns>Nos devolverá los CENTIMETROS CUADRADOS que le corresponden
        /// en PIES CUADRADOS ingresados por el usuario</returns>
        public decimal convPieCuadACmCuad(double pie)
        {
            return decimal.Parse(pie.ToString()) * 929m;
        }
        /// <summary>
        /// Me va a permitir obtener las YARDAS CUADRADAS que le corresponderán a los
        /// METROS CUADRADOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS CUADRADOS que deseo convertir
        /// a YARDAS CUADRADAS</param>
        /// <returns>Nos devolverá las YARDAS CUADRADAS que le corresponden
        /// en METROS CUADRADOS ingresados por el usuario</returns>
        public decimal convMtCuadAYardCuad(double mt)
        {
            return decimal.Parse(mt.ToString()) * 1.196m;
        }
        /// <summary>
        /// Me va a permitir obtener los METROS CUADRADOS que le corresponderán a las
        /// YARDAS CUADRADAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="yard">Las YARDAS CUADRADAS que deseo convertir
        /// a METROS CUADRADOS</param>
        /// <returns>Nos devolverá los METROS CUADRADOS que le corresponden
        /// en YARDAS CUADRADAS ingresados por el usuario</returns>
        public decimal convYardCuadAMtCuad(double yard)
        {
            return decimal.Parse(yard.ToString()) / 1.196m;
        }
        /// <summary>
        /// Me permite hacer una conversión de METROS CUADRADOS ingresados
        /// por parametro a CENTIMETROS CUADRADOS.
        /// </summary>
        /// <param name="cm">CENTIMETROS CUADRADOS que se convertirán en
        /// METROS CUADRADOS</param>
        /// <returns>METROS CUADRADOS correspondientes a la conversión</returns>
        public decimal convCmCuadAMtCuad(double cm)
        {
            return decimal.Parse(cm.ToString()) / 10000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de CENTIMETROS CUADRADOS ingresados
        /// por parametro a METROS CUADRADOS.
        /// </summary>
        /// <param name="mt">METROS CUADRADOS que se convertirán en
        /// CENTIMETROS CUADRADOS</param>
        /// <returns>CENTIMETROS CUADRADOS correspondientes a la conversión</returns>
        public decimal convMtCuadACmCuad(double mt)
        {
            return decimal.Parse(mt.ToString()) * 10000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTAREAS ingresados
        /// por parametro a METROS CUADRADOS.
        /// </summary>
        /// <param name="mt">METROS CUADRADOS que se convertirán en
        /// HECTAREAS</param>
        /// <returns>HECTAREAS correspondientes a la conversión</returns>
        public decimal convMtCuadAHect(double mt)
        {
            return decimal.Parse(mt.ToString()) / 10000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de METROS CUADRADOS ingresados
        /// por parametro a HECTAREAS.
        /// </summary>
        /// <param name="hect">HECTAREAS que se convertirán en
        /// METROS CUADRADOS</param>
        /// <returns>METROS CUADRADOS correspondientes a la conversión</returns>
        public decimal convHectAMtCuad(double hect)
        {
            return decimal.Parse(hect.ToString()) * 10000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de KILOMETROS CUADRADOS ingresados
        /// por parametro a HECTAREAS.
        /// </summary>
        /// <param name="hect">HECTAREAS que se convertirán en
        /// KILOMETROS CUADRADOS</param>
        /// <returns>KILOMETROS CUADRADOS correspondientes a la conversión</returns>
        public decimal convHectAKmCuad(double hect)
        {
            return decimal.Parse(hect.ToString()) / 100m;

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTAREAS ingresados
        /// por parametro a KILOMETROS CUADRADOS.
        /// </summary>
        /// <param name="km">KILOMETROS CUADRADOS que se convertirán en
        /// HECTAREAS</param>
        /// <returns>HECTAREAS correspondientes a la conversión</returns>
        public decimal convKmCuadAHect(double km)
        {
            return decimal.Parse(km.ToString()) * 100m;

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTAREAS ingresados
        /// por parametro a KILOMETROS CUADRADOS.
        /// </summary>
        /// <param name="km">KILOMETROS CUADRADOS que se convertirán en
        /// HECTAREAS</param>
        /// <returns>HECTAREAS correspondientes a la conversión</returns>
        public decimal convMtCuadAPie(double mt)
        {
            return decimal.Parse(mt.ToString()) * 10.764m;

        }
        /// <summary>
        /// Me permite hacer una conversión de METROS CUADRADOS ingresados
        /// por parametro a PIES CUADRADOS.
        /// </summary>
        /// <param name="pie">PIES CUADRADOS que se convertirán en
        /// METROS CUADRADOS</param>
        /// <returns>METROS CUADRADOS correspondientes a la conversión</returns>
        public decimal convPieCuadAMtCuad(double pie)
        {
            return decimal.Parse(pie.ToString()) / 10.764m;

        }
        /// <summary>
        /// Me permite hacer una conversión de YARDAS CUADRADOS ingresados
        /// por parametro a METROS CUADRADOS.
        /// </summary>
        /// <param name="mt">METROS CUADRADOS que se convertirán en
        /// YARDAS CUADRADOS</param>
        /// <returns>YARDAS CUADRADOS correspondientes a la conversión</returns>
        public decimal convMtCuadAYrdCuad(double mt)
        {
            return decimal.Parse(mt.ToString()) * 1.196m;

        }
        /// <summary>
        /// Me permite hacer una conversión de METROS CUADRADOS ingresados
        /// por parametro a YARDAS CUADRADAS.
        /// </summary>
        /// <param name="yrd">YARDAS CUADRADAS que se convertirán en
        /// METROS CUADRADOS</param>
        /// <returns>METROS CUADRADOS correspondientes a la conversión</returns>
        public decimal convYrdCuadAMtCuad(double yrd)
        {
            return decimal.Parse(yrd.ToString()) / 1.196m;

        }
        /// <summary>
        /// Me permite hacer una conversión de ACRE ingresados
        /// por parametro a METROS CUADRADOS.
        /// </summary>
        /// <param name="mt">METROS CUADRADOS que se convertirán en
        /// ACRE</param>
        /// <returns>ACRE correspondientes a la conversión</returns>
        public decimal convMtCuadAAcre(double mt)
        {
            return decimal.Parse(mt.ToString()) / 4047m;

        }
        /// <summary>
        /// Me permite hacer una conversión de METROS CUADRADOS ingresados
        /// por parametro a ACRE.
        /// </summary>
        /// <param name="acre">ACRE que se convertirán en
        /// METROS CUADRADOS</param>
        /// <returns>METROS CUADRADOS correspondientes a la conversión</returns>
        public decimal convAcreAMtCuad(double acre)
        {
            return decimal.Parse(acre.ToString()) * 4047m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES CUADRADOS ingresados
        /// por parametro a HECTAREA.
        /// </summary>
        /// <param name="hect">HECTAREA que se convertirán en
        /// PIES CUADRADOS</param>
        /// <returns>PIES CUADRADOS correspondientes a la conversión</returns>
        public decimal convHectAPieCuad(double hect)
        {
            return decimal.Parse(hect.ToString()) * 107639m;

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTAREAS ingresados
        /// por parametro a PIES CUADRADOS.
        /// </summary>
        /// <param name="pie">PIES CUADRADOS que se convertirán en
        /// HECTAREAS</param>
        /// <returns>HECTAREAS correspondientes a la conversión</returns>
        public decimal convPieCuadAHect(double pie)
        {
            return decimal.Parse(pie.ToString()) / 107639m;

        }
        /// <summary>
        /// Me permite hacer una conversión de YARDAS CUADRADAS ingresados
        /// por parametro a HECTAREAS.
        /// </summary>
        /// <param name="hect">HECTAREAS que se convertirán en
        /// YARDAS CUADRADAS</param>
        /// <returns>YARDAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convHectAYrdCuad(double hect)
        {
            return decimal.Parse(hect.ToString()) * 11960m;

        }
        /// <summary>
        /// Me permite hacer una conversión de HECTAREAS ingresados
        /// por parametro a YARDAS CUADRADAS.
        /// </summary>
        /// <param name="yrd">YARDAS CUADRADAS que se convertirán en
        /// HECTAREAS</param>
        /// <returns>HECTAREAS correspondientes a la conversión</returns>
        public decimal convYrdCuadAHect(double yrd)
        {
            return decimal.Parse(yrd.ToString()) / 11960m;

        }
        /// <summary>
        /// Me permite hacer una conversión de YARDAS CUADRADAS ingresados
        /// por parametro a KILOMETROS CUADRADOS.
        /// </summary>
        /// <param name="km">KILOMETROS CUADRADOS que se convertirán en
        /// YARDAS CUADRADAS</param>
        /// <returns>YARDAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convKmCuadAYrdCuad(double km)
        {
            return decimal.Parse(km.ToString()) * 0.000001196m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUADRADAS ingresados
        /// por parametro a PIES CUADRADOS.
        /// </summary>
        /// <param name="pie">PIES CUADRADOS que se convertirán en
        /// PULGADAS CUADRADAS</param>
        /// <returns>PULGADAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convPieCuadAPlgCuad(double pie)
        {
            return decimal.Parse(pie.ToString()) * 144m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES CUADRADAS ingresados
        /// por parametro a PULGADAS CUADRADOS.
        /// </summary>
        /// <param name="plg">PULGADAS CUADRADOS que se convertirán en
        /// PIES CUADRADAS</param>
        /// <returns>PIES CUADRADAS correspondientes a la conversión</returns>
        public decimal convPlgCuadAPieCuad(double plg)
        {
            return decimal.Parse(plg.ToString()) / 144m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUADRADAS ingresados
        /// por parametro a YARDAS CUADRADOS.
        /// </summary>
        /// <param name="yrd">YARDAS CUADRADOS que se convertirán en
        /// PULGADAS CUADRADAS</param>
        /// <returns>PULGADAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convYrdCuadAPlgCuad(double yrd)
        {
            return decimal.Parse(yrd.ToString()) * 1296;

        }
        /// <summary>
        /// Me permite hacer una conversión de YARDAS CUADRADAS ingresados
        /// por parametro a PULGADAS CUADRADOS.
        /// </summary>
        /// <param name="plg">PULGADAS CUADRADOS que se convertirán en
        /// YARDAS CUADRADAS</param>
        /// <returns>YARDAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convPlgCuadAYrdCuad(double plg)
        {
            return decimal.Parse(plg.ToString()) / 1296m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUADRADAS ingresados
        /// por parametro a ACRE.
        /// </summary>
        /// <param name="acre">ACRE que se convertirán en
        /// PULGADAS CUADRADAS</param>
        /// <returns>PULGADAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convAcreAPlgCuad(double acre)
        {
            return decimal.Parse(acre.ToString()) * 6273000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de ACRE ingresados
        /// por parametro a PULGADAS CUADRADAS.
        /// </summary>
        /// <param name="plg">PULGADAS CUADRADAS que se convertirán en
        /// ACRE</param>
        /// <returns>ACRE correspondientes a la conversión</returns>
        public decimal convPlgCuadAAcre(double plg)
        {
            return decimal.Parse(plg.ToString()) / 6273000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES CUADRADOS ingresados
        /// por parametro a ACRE.
        /// </summary>
        /// <param name="acre">ACRE que se convertirán en
        /// PIES CUADRADOS</param>
        /// <returns>PIES CUADRADOS correspondientes a la conversión</returns>
        public decimal convAcreAPieCuad(double acre)
        {
            return decimal.Parse(acre.ToString()) * 43560m;

        }
        /// <summary>
        /// Me permite hacer una conversión de ACRE ingresados
        /// por parametro a PIES CUADRADOS.
        /// </summary>
        /// <param name="pie">PIES CUADRADOS que se convertirán en
        /// ACRE</param>
        /// <returns>ACRE CUADRADOS correspondientes a la conversión</returns>
        public decimal convPieCuadAAcre(double pie)
        {
            return decimal.Parse(pie.ToString()) / 43560m;

        }
        /// <summary>
        /// Me permite hacer una conversión de YARDAS CUADRADAS ingresados
        /// por parametro a ACRE.
        /// </summary>
        /// <param name="acre">ACRE  que se convertirán en
        /// YARDAS CUADRADAS</param>
        /// <returns>YARDAS CUADRADOS correspondientes a la conversión</returns>
        public decimal convAcreAYrdCuad(double acre)
        {
            return decimal.Parse(acre.ToString()) * 4840m;

        }
        /// <summary>
        /// Me permite hacer una conversión de ACRE ingresados
        /// por parametro a YARDAS CUADRADAS.
        /// </summary>
        /// <param name="yrd">YARDAS CUADRADAS que se convertirán en
        /// ACRE</param>
        /// <returns>ACRE correspondientes a la conversión</returns>
        public decimal convYrdCuadAAcre(double yrd)
        {
            return decimal.Parse(yrd.ToString()) / 4840m;

        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS CUADRADAS ingresados
        /// por parametro a ACRE.
        /// </summary>
        /// <param name="acre">ACRE que se convertirán en
        /// MILLAS CUADRADAS</param>
        /// <returns>MILLAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convAcreAMillaCuad(double acre)
        {
            return ddecimal.Parse(acre.ToString()) / 640m;

        }
        /// <summary>
        /// Me permite hacer una conversión de ACRE  ingresados
        /// por parametro a MILLAS CUADRADAS.
        /// </summary>
        /// <param name="milla">MILLAS CUADRADAS que se convertirán en
        /// ACRE</param>
        /// <returns>ACRE correspondientes a la conversión</returns>
        public decimal convMillaCuadAAcre(double milla)
        {
            return decimal.Parse(milla.ToString()) * 640m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PULGADAS CUADRADAS ingresados
        /// por parametro a MILLAS CUADRADAS.
        /// </summary>
        /// <param name="milla">MILLAS CUADRADAS que se convertirán en
        /// PULGADAS CUADRADAS</param>
        /// <returns>PULGADAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convMillaCuadAPlgCuad(double milla)
        {
            return decimal.Parse(milla.ToString()) * 4014000000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS CUADRADAS ingresados
        /// por parametro a PULGADAS CUADRADAS.
        /// </summary>
        /// <param name="plg">PULGADAS CUADRADAS que se convertirán en
        /// MILLAS CUADRADAS</param>
        /// <returns>MILLAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convPlgCuadAMillaCuad(double plg)
        {
            return decimal.Parse(plg.ToString()) / 4014000000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIES CUADRADAS ingresados
        /// por parametro a MILLAS CUADRADAS.
        /// </summary>
        /// <param name="milla">MILLAS CUADRADAS que se convertirán en
        /// PIES CUADRADAS</param>
        /// <returns>PIES CUADRADAS correspondientes a la conversión</returns>
        public decimal convMillaCuadAPieCuad(double milla)
        {
            return decimal.Parse(milla.ToString()) * 27880000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS CUADRADAS ingresados
        /// por parametro a PIES CUADRADAS.
        /// </summary>
        /// <param name="pie">PIES CUADRADAS que se convertirán en
        /// MILLAS CUADRADAS</param>
        /// <returns>MILLAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convPieCuadAMillaCuad(double pie)
        {
            return decimal.Parse(pie.ToString()) / 27880000;

        }
        /// <summary>
        /// Me permite hacer una conversión de MILLAS CUADRADAS ingresados
        /// por parametro a YARDAS CUADRADAS.
        /// </summary>
        /// <param name="yrd">YARDAS CUADRADAS que se convertirán en
        /// MILLAS CUADRADAS</param>
        /// <returns>MILLAS CUADRADAS correspondientes a la conversión</returns>
        public decimal convYrdCuadAMillaCuad(double yrd)
        {
            return decimal.Parse(yrd.ToString()) * 0.00000032283m;

        }

    }
}
