﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1LOC Base
//*************************
// Bloque de Código fuente:
// Clase Masa
// Breve: incluye todos los métodos de de conversiones
//de entre el sistema métrico y del sistema imperial, de 
//unidades de MASA
//*************************
//TODO: terminarlo
//UNDONE
//HACK
namespace ConversionDeUnidades
{
    public class Masa
    {
        /// <summary>
        /// Constructor para hacer conversiones de Masa
        /// entre sistema métrico e imperial.
        /// </summary>
        public Masa()
        {

        }
        /// <summary>
        /// Me va a permitir obtener las ONZAS que le corresponderán a los
        /// MILIGRAMOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mg">Los MILIGRAMOS que deseo convertir
        /// a ONZAS</param>
        /// <returns>Nos devolverá las ONZAS que le corresponden
        /// en MILIGRAMOS ingresados por el usuario</returns>
        public decimal convMgAOz(double mg)
        {
            return decimal.Parse(mg.ToString()) * 0.000035274m;
        }
        /// <summary>
        /// Me va a permitir obtener los MILIGRAMOS que le corresponderán a las
        /// ONZAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="oz">Las ONZAS que deseo convertir
        /// a MILIGRAMOS</param>
        /// <returns>Nos devolverá los MILIGRAMOS que le corresponden
        /// en ONZAS ingresados por el usuario</returns>
        public decimal convOzAMg(double oz)
        {
            return decimal.Parse(oz.ToString()) * 28350m;
        }
        /// <summary>
        /// Me va a permitir obtener las ONZAS que le corresponderán a los
        /// GRAMOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="gr">Los GRAMOS que deseo convertir
        /// a ONZAS</param>
        /// <returns>Nos devolverá las ONZAS que le corresponden
        /// en GRAMOS ingresados por el usuario</returns>
        public decimal convGrAOz(double gr)
        {
            return decimal.Parse(gr.ToString()) / 28.35m;
        }
        /// <summary>
        /// Me va a permitir obtener los GRAMOS que le corresponderán a las
        /// ONZAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="oz">Las ONZAS que deseo convertir
        /// a GRAMOS</param>
        /// <returns>Nos devolverá los GRAMOS que le corresponden
        /// en ONZAS ingresados por el usuario</returns>
        public decimal convOzAGr(double oz)
        {
            return decimal.Parse(oz.ToString()) * 28.35m;
        }
        /// <summary>
        /// Me va a permitir obtener las ONZAS que le corresponderán a los
        /// KILOGRAMOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="kg">Los KILOGRAMOS que deseo convertir
        /// a ONZAS</param>
        /// <returns>Nos devolverá las ONZAS que le corresponden
        /// en KILOGRAMOS ingresados por el usuario</returns>
        public decimal convKgAOz(double kg)
        {
            return decimal.Parse(kg.ToString()) * 35.274m;
        }
        /// <summary>
        /// Me va a permitir obtener los KILOGRAMOS que le corresponderán a las
        /// ONZAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="oz">Las ONZAS que deseo convertir
        /// a KILOGRAMOS</param>
        /// <returns>Nos devolverá los KILOGRAMOS que le corresponden
        /// en ONZAS ingresados por el usuario</returns>
        public decimal convOzAKg(double oz)
        {
            return decimal.Parse(oz.ToString()) / 35.274m;
        }
        /// <summary>
        /// Me va a permitir obtener las ONZAS que le corresponderán a las
        /// TONELADAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="ton">Las TONELADAS que deseo convertir
        /// a ONZAS</param>
        /// <returns>Nos devolverá las ONZAS que le corresponden
        /// en TONELADAS ingresados por el usuario</returns>
        public decimal convTonAOz(double ton)
        {
            return decimal.Parse(ton.ToString()) * 35274m;
        }
        /// <summary>
        /// Me va a permitir obtener las TONELADAS que le corresponderán a las
        /// ONZAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="oz">Las ONZAS que deseo convertir
        /// a TONELADAS</param>
        /// <returns>Nos devolverá las TONELADAS que le corresponden
        /// en ONZAS ingresados por el usuario</returns>
        public decimal convOzATon(double oz)
        {
            return decimal.Parse(oz.ToString()) / 35274m;
        }
        /// <summary>
        /// Me va a permitir obtener las LIBRAS que le corresponderán a los
        /// MILIGRAMOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mg">Los MILIGRAMOS que deseo convertir
        /// a LIBRAS</param>
        /// <returns>Nos devolverá las LIBRAS que le corresponden
        /// en Miligramos ingresados por el usuario</returns>
        public decimal convMgALb(double mg)
        {
            return decimal.Parse(mg.ToString()) / 453592m;
        }
        /// <summary>
        /// Me va a permitir obtener los MILIGRAMOS que le corresponderán a las
        /// LIBRAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="lb">Los MILIGRAMOS que deseo convertir
        /// a LIBRAS</param>
        /// <returns>Nos devolverá los MILIGRAMOS que le corresponden
        /// en LIBRAS ingresados por el usuario</returns>
        public decimal convLbAMg(double lb)
        {
            return decimal.Parse(lb.ToString()) * 453592m;
        }
        /// <summary>
        /// Me va a permitir obtener las LIBRAS que le corresponderán a los
        /// GRAMOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="gr">Los GRAMOS que deseo convertir
        /// a LIBRAS</param>
        /// <returns>Nos devolverá las LIBRAS que le corresponden
        /// en GRAMOS ingresados por el usuario</returns>
        public decimal convGrALb(double gr)
        {
            return decimal.Parse(gr.ToString()) / 454m;
        }
        /// <summary>
        /// Me va a permitir obtener los GRAMOS que le corresponderán a los
        /// LIBRAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="lb">Las LIBRAS que deseo convertir
        /// a GRAMOS</param>
        /// <returns>Nos devolverá los GRAMOS que le corresponden
        /// en LIBRAS ingresados por el usuario</returns>
        public decimal convLbAGr(double lb)
        {
            return decimal.Parse(lb.ToString()) * 454m;
        }
        /// <summary>
        /// Me va a permitir obtener las LIBRAS que le corresponderán a los
        /// KILOGRAMOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="kg">Los KILOGRAMOS que deseo convertir
        /// a LIBRAS</param>
        /// <returns>Nos devolverá las LIBRAS que le corresponden
        /// en KILOGRAMOS ingresados por el usuario</returns>
        public decimal convKgALb(double kg)
        {
            return decimal.Parse(kg.ToString()) * 2.205m;
        }
        /// <summary>
        /// Me va a permitir obtener los KILOGRAMOS que le corresponderán a las
        /// LIBRAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="lb">Las LIBRAS que deseo convertir
        /// a KILOGRAMOS</param>
        /// <returns>Nos devolverá los KILOGRAMOS que le corresponden
        /// en LIBRAS ingresados por el usuario</returns>
        public decimal convLbAKg(double lb)
        {
            return decimal.Parse(lb.ToString()) / 2.205m;
        }
        /// <summary>
        /// Me va a permitir obtener las LIBRAS que le corresponderán a las
        /// TONELADAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="ton">Las TONELADAS que deseo convertir
        /// a LIBRAS</param>
        /// <returns>Nos devolverá las LIBRAS que le corresponden
        /// en TONELADAS ingresados por el usuario</returns>
        public decimal convTonALb(double ton)
        {
            return decimal.Parse(ton.ToString()) * 2205m;
        }
        /// <summary>
        /// Me va a permitir obtener las TONELADAS que le corresponderán a las
        /// LIBRAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="lb">Las LIBRAS que deseo convertir
        /// a TONELADAS</param>
        /// <returns>Nos devolverá las TONELADAS que le corresponden
        /// en LIBRAS ingresados por el usuario</returns>
        public decimal convLbATon(double lb)
        {
            return decimal.Parse(lb.ToString()) / 2205m;
        }
        /// <summary>
        /// Me permite hacer una conversión de PIEDRA ingresados
        /// por parametro a MILIGRAMOS.
        /// </summary>
        /// <param name="mg">MILIGRAMOS que se convertirán en
        /// PIEDRA</param>
        /// <returns>PIEDRA correspondientes a la conversión</returns>
        public decimal convMgAPiedra(double mg)
        {
            return decimal.Parse(mg.ToString()) / 0.00000635m;

        }
        /// <summary>
        /// Me permite hacer una conversión de MILIGRAMOS ingresados
        /// por parametro a PIEDRA.
        /// </summary>
        /// <param name="piedra">PIEDRA que se convertirán en
        /// MILIGRAMOS</param>
        /// <returns>MILIGRAMOS correspondientes a la conversión</returns>
        public decimal convPiedraAMg(double piedra)
        {
            return decimal.Parse(piedra.ToString()) * 0.00000635m;

        }
        /// <summary>
        /// Me permite hacer una conversión de MILIGRAMOS ingresados
        /// por parametro a CENTENAS .
        /// </summary>
        /// <param name="cent">CENTENAS que se convertirán en
        /// MILIGRAMOS</param>
        /// <returns>MILIGRAMOS correspondientes a la conversión</returns>
        public decimal convCentAMg(double cent)
        {
            return decimal.Parse(cent.ToString()) * 50800000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de CENTENAS ingresados
        /// por parametro a MILIGRAMOS.
        /// </summary>
        /// <param name="mg">MILIGRAMOS que se convertirán en
        /// CENTENAS</param>
        /// <returns>CENTENAS correspondientes a la conversión</returns>
        public decimal convMgACent(double mg)
        {
            return decimal.Parse(mg.ToString()) / 50800000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de TOENLADAS ingresados
        /// por parametro a MILIGRAMOS .
        /// </summary>
        /// <param name="mg">MILIGRAMOS que se convertirán en
        /// TONELADAS</param>
        /// <returns>TONELADAS correspondientes a la conversión</returns>
        public decimal convMgATon(double mg)
        {
            return decimal.Parse(mg.ToString()) / 1000000000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de MILIGRAMOS ingresados
        /// por parametro a TONELADAS.
        /// </summary>
        /// <param name="ton">TONELADAS que se convertirán en
        /// MILIGRAMOS</param>
        /// <returns>MILIGRAMOS correspondientes a la conversión</returns>
        public decimal convTonAMg(double ton)
        {
            return decimal.Parse(ton .ToString())* 1000000000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIEDRAS ingresados
        /// por parametro a GRAMOS.
        /// </summary>
        /// <param name="gr">GRAMOS que se convertirán en
        /// PIEDRAS</param>
        /// <returns>PIEDRAS correspondientes a la conversión</returns>
        public decimal convGrAPiedra(double gr)
        {
            return decimal.Parse(gr.ToString()) / 6350m;

        }
        /// <summary>
        /// Me permite hacer una conversión de GRAMOS ingresados
        /// por parametro a PIEDRA .
        /// </summary>
        /// <param name="piedra">PEDRA que se convertirán en
        /// GRAMOS</param>
        /// <returns>GRAMOS correspondientes a la conversión</returns>
        public decimal convPiedraAGr(double piedra)
        {
            return decimal.Parse(piedra.ToString()) * 6350m;

        }
        /// <summary>
        /// Me permite hacer una conversión de CENTENAS ingresados
        /// por parametro a GRAMOS .
        /// </summary>
        /// <param name="gr">GRAMOS que se convertirán en
        /// CENTENAS</param>
        /// <returns>CENTENAS correspondientes a la conversión</returns>
        public decimal convGrACent(double gr)
        {
            return decimal.Parse(gr.ToString()) / 50802m;

        }
        /// <summary>
        /// Me permite hacer una conversión de GRAMOS ingresados
        /// por parametro a CENTENAS .
        /// </summary>
        /// <param name="cent">CENTENAS que se convertirán en
        /// GRAMOS</param>
        /// <returns>GRAMOS correspondientes a la conversión</returns>
        public decimal convCentAGr(double cent)
        {
            return decimal.Parse(cent.ToString()) * 50802m;

        }
        /// <summary>
        /// Me permite hacer una conversión de TONELADAS ingresados
        /// por parametro a GRAMOS.
        /// </summary>
        /// <param name="gr">GRAMOS que se convertirán en
        /// TONELADAS</param>
        /// <returns>TONELADAS correspondientes a la conversión</returns>
        public decimal convGrATon(double gr)
        {
            return decimal.Parse(gr.ToString()) / 1000000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de GRAMOS ingresados
        /// por parametro a TONELADAS.
        /// </summary>
        /// <param name="ton">TONELADAS que se convertirán en
        /// GRAMOS</param>
        /// <returns>GRAMOS correspondientes a la conversión</returns>
        public decimal convTonAGr(double ton)
        {
            return decimal.Parse(ton.ToString()) * 1000000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de GRAMOS ingresados
        /// por parametro a KILOGRAMOS .
        /// </summary>
        /// <param name="kg">KILOGRAMOS que se convertirán en
        /// GRAMOS</param>
        /// <returns>GRAMOS correspondientes a la conversión</returns>
        public decimal convKgAGr(double kg)
        {
            return decimal.Parse(kg.ToString()) * 1000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de KILOGRAMOS ingresados
        /// por parametro a GRAMOS .
        /// </summary>
        /// <param name="gr">GRAMOS que se convertirán en
        /// KILOGRAMOS</param>
        /// <returns>KILOGRAMOS correspondientes a la conversión</returns>
        public decimal convGrAKg(double gr)
        {
            return decimal.Parse(gr.ToString()) / 1000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIEDRA ingresados
        /// por parametro a KILOGRAMOS .
        /// </summary>
        /// <param name="kg">KILOGRAMOS que se convertirán en
        /// PIEDRA</param>
        /// <returns>PIEDRA correspondientes a la conversión</returns>
        public decimal convKgAPiedra(double kg)
        {
            return decimal.Parse(kg.ToString()) / 6.35m;

        }
        /// <summary>
        /// Me permite hacer una conversión de KILOGRAMOS ingresados
        /// por parametro a PIEDRA .
        /// </summary>
        /// <param name="piedra">PIEDRA que se convertirán en
        /// KILOGRAMOS</param>
        /// <returns>ONZAS correspondientes a la conversión</returns>
        public decimal convPiedraAKg(double piedra)
        {
            return decimal.Parse(piedra.ToString()) * 6.35m;

        }
        /// <summary>
        /// Me permite hacer una conversión de CENTENAS ingresados
        /// por parametro a KILOGRAMOS .
        /// </summary>
        /// <param name="kg">KILOGRAMOS que se convertirán en
        /// CENTENAS</param>
        /// <returns>CENTENAS correspondientes a la conversión</returns>
        public decimal convKgACent(double kg)
        {
            return decimal.Parse(kg.ToString()) / 50.802m;

        }
        /// <summary>
        /// Me permite hacer una conversión de KILOGRAMOS ingresados
        /// por parametro a CENTENAS .
        /// </summary>
        /// <param name="cent">CENTENAS que se convertirán en
        /// KILOGRAMOS</param>
        /// <returns>KILOGRAMOS correspondientes a la conversión</returns>
        public decimal convCentAKg(double cent)
        {
            return decimal.Parse(cent.ToString()) * 50.802m;

        }
        /// <summary>
        /// Me permite hacer una conversión de TONELADAS ingresados
        /// por parametro a KILOGRAMOS .
        /// </summary>
        /// <param name="kg">KILOGRAMOS que se convertirán en
        /// TONELADAS</param>
        /// <returns>TONELADAS correspondientes a la conversión</returns>
        public decimal convKgATon(double kg)
        {
            return decimal.Parse(kg.ToString()) / 1000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de KILOGRAMOS ingresados
        /// por parametro a TONELADAS .
        /// </summary>
        /// <param name="ton">TONELADAS que se convertirán en
        /// KILOGRAMOS</param>
        /// <returns>KILOGRAMOS correspondientes a la conversión</returns>
        public decimal convTonAKg(double ton)
        {
            return decimal.Parse(ton.ToString()) * 1000m;

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS ingresados
        /// por parametro a LIBRAS .
        /// </summary>
        /// <param name="lb">LIBRAS que se convertirán en
        /// ONZAS</param>
        /// <returns>ONZAS correspondientes a la conversión</returns>
        public decimal convLbAOz(double lb)
        {
            return decimal.Parse(lb.ToString()) * 16m;

        }
        /// <summary>
        /// Me permite hacer una conversión de LIBRAS ingresados
        /// por parametro a ONZAS .
        /// </summary>
        /// <param name="oz">ONZAS que se convertirán en
        /// LIBRAS</param>
        /// <returns>LIBRAS correspondientes a la conversión</returns>
        public decimal conOzALb(double oz)
        {
            return decimal.Parse(oz.ToString()) / 16m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIEDRA ingresados
        /// por parametro a LIBRAS .
        /// </summary>
        /// <param name="lb">LIBRAS que se convertirán en
        /// PIEDRAS</param>
        /// <returns>PIEDRA correspondientes a la conversión</returns>
        public decimal convLbAPiedra(double lb)
        {
            return decimal.Parse(lb.ToString()) / 14m;

        }
        /// <summary>
        /// Me permite hacer una conversión de LIBRAS ingresados
        /// por parametro a PIEDRA .
        /// </summary>
        /// <param name="piedra">PIEDRA que se convertirán en
        /// LIBRAS</param>
        /// <returns>LIBRAS correspondientes a la conversión</returns>
        public decimal convPiedraALb(double piedra)
        {
            return decimal.Parse(piedra.ToString()) * 14m;

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS ingresados
        /// por parametro a PIEDRA .
        /// </summary>
        /// <param name="piedra">PIEDRA que se convertirán en
        /// ONZAS</param>
        /// <returns>ONZAS correspondientes a la conversión</returns>
        public decimal convPiedraAOz(double piedra)
        {
            return decimal.Parse(piedra.ToString()) * 224m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIEDRA ingresados
        /// por parametro a ONZAS .
        /// </summary>
        /// <param name="oz">ONZAS que se convertirán en
        /// PIEDRA</param>
        /// <returns>PIEDRA correspondientes a la conversión</returns>
        public decimal convOzAPiedra(double oz)
        {
            return decimal.Parse(oz.ToString()) / 224m;

        }
        /// <summary>
        /// Me permite hacer una conversión de CENTENAS ingresados
        /// por parametro a PIEDRA .
        /// </summary>
        /// <param name="piedra">PIEDRA que se convertirán en
        /// CENTENAS</param>
        /// <returns>CENTENAS correspondientes a la conversión</returns>
        public decimal convPiedraACent(double piedra)
        {
            return decimal.Parse(piedra.ToString()) / 8m;

        }
        /// <summary>
        /// Me permite hacer una conversión de PIEDRA ingresados
        /// por parametro a CENTENAS .
        /// </summary>
        /// <param name="cent">CENTENAS que se convertirán en
        /// PIEDRAS</param>
        /// <returns>PIEDRAS correspondientes a la conversión</returns>
        public decimal convCentAPiedra(double cent)
        {
            return decimal.Parse(cent.ToString()) * 8m;

        }
        /// <summary>
        /// Me permite hacer una conversión de ONZAS ingresados
        /// por parametro a CENTENAS .
        /// </summary>
        /// <param name="cent">CENTENAS que se convertirán en
        /// ONZAS</param>
        /// <returns>ONZAS correspondientes a la conversión</returns>
        public decimal convCentAOz(double cent)
        {
            return decimal.Parse(cent.ToString()) * 1792m;

        }
        /// <summary>
        /// Me permite hacer una conversión de CENTENAS ingresados
        /// por parametro a ONZAS .
        /// </summary>
        /// <param name="oz">ONZAS que se convertirán en
        /// CENTENAS</param>
        /// <returns>CENTENAS correspondientes a la conversión</returns>
        public decimal convOzACent(double oz)
        {
            return decimal.Parse(oz.ToString()) / 1792m;

        }
        /// <summary>
        /// Me permite hacer una conversión de CENTENAS ingresados
        /// por parametro a TONELADAS .
        /// </summary>
        /// <param name="ton">TONELADAS que se convertirán en
        /// CENTENAS</param>
        /// <returns>CENTENAS correspondientes a la conversión</returns>
        public decimal convTonACent(double ton)
        {
            //return decimal.Parse(ton * 19.68);
            return decimal.Parse(ton.ToString()) * 19.684m;

        }
        /// <summary>
        /// Me permite hacer una conversión de TONELADAS ingresados
        /// por parametro a CENTENAS .
        /// </summary>
        /// <param name="cent">CENTENAS que se convertirán en
        /// TONELADAS</param>
        /// <returns>TONELADAS correspondientes a la conversión</returns>
        public decimal convCentATon(double cent)
        {
            return decimal.Parse(cent.ToString()) / 19.684m;

        }

    }
}
