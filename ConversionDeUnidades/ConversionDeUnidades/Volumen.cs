﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1LOC Base
//*************************
// Bloque de Código fuente:
// Clase Volumen
// Breve: incluye todos los métodos de de conversiones
//de entre el sistema métrico y del sistema imperial, de 
//unidades de VOLUMEN
//*************************
//TODO: terminarlo.
//UNDONE
//HACK
namespace ConversionDeUnidades
{
    public class Volumen
    {
        /// <summary>
        /// Constructor para hacer conversiones de Volumen
        /// entre sistema métrico e imperial.
        /// </summary>
        public Volumen()
        {

        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// CENTIMETROS CUBICOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="cm">Los CENTIMETROS CUBICOS que deseo convertir
        /// a CENTIMETROS CUBICOS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en CENTIMETROS CUBICOS ingresados por el usuario</returns>
        public decimal convCmCubAPlgCub(double cm)
        {
            return decimal.Parse(cm / 16.387).ToString());
        }

        public decimal convCmCubAPieCub(double cm)
        {
            return convCmCubAPlgCub(cm)*1728;
        }

        public decimal convCmCubAOz(double cm)
        {
            return convCmCubAPieCub(cm) * decimal.Parse(0.0010033984665586955.ToString());
        }

        public decimal convCmCubAPint(double cm)
        {
            return convCmCubAOz(cm)*20;
        }

        public decimal convCmCubAGal(double cm)
        {
            return convCmCubAPint(cm) * 8;
        }

        /// <summary>
        /// Me va a permitir obtener los CENTIMETROS CUBICOS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a CENTIMETROS CUBICOS</param>
        /// <returns>Nos devolverá las CENTIMETROS CUBICOS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario</returns>
        public decimal convPlgCubACmCub(double plg)
        {
            return decimal.Parse(plg * 16.387).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// DECIMETROS CUBICOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="dcm">Los DECIMETROS CUBICOS que deseo convertir
        /// a PULGADAS CUBICAS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en DECIMETROS CUBICOS ingresados por el usuario</returns>
        public decimal convDcmCubAPlgCub(double dcm)
        {
            return decimal.Parse(dcm * 61.024).ToString());
            //return decimal.Parse((dcm*0.0353)*1728)+"");
        }
        /// <summary>
        /// Me va a permitir obtener los DECIMETROS CUBICOS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a DECIMETROS CUBICOS</param>
        /// <returns>Nos devolverá los DECIMETROS CUBICOS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario</returns>
        public decimal convPlgCubADcmCub(double plg)
        {
            return decimal.Parse(plg / 61.024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// METROS CUBICOS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="mt">Los METROS CUBICOS que deseo convertir
        /// a PULGADAS CUBICAS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en METROS CUBICOS ingresados por el usuario</returns>
        public decimal convMtCubAPlgCub(double mt)
        {
            return decimal.Parse(mt * 61024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los METROS CUBICOS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a METROS CUBICOS</param>
        /// <returns>Nos devolverá los METROS CUBICOS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario</returns>
        public decimal convPlgCubAMtCub(double plg)
        {
            return decimal.Parse(plg * 0.000016387).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// LITROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="lt">Los LITROS que deseo convertir
        /// a PULGADAS CUBICAS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en LITROS ingresados por el usuario</returns>
        public decimal convLtAPlgCub(double lt)
        {
            return decimal.Parse(lt * 61.024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los LITROS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a LITROS</param>
        /// <returns>Nos devolverá los LITROS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario</returns>
        public decimal convPlgCubALt(double plg)
        {
            return decimal.Parse(plg / 61.024).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener las PULGADAS CUBICAS que le corresponderán a los
        /// HECTOLITROS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="hect">Los HECTOLITROS que deseo convertir
        /// a PULGADAS CUBICAS</param>
        /// <returns>Nos devolverá las PULGADAS CUBICAS que le corresponden
        /// en HECTOLITROS ingresados por el usuario</returns>
        public decimal convHectAPlgCub(double hect)
        {
            return decimal.Parse(hect * 6102).ToString());
        }
        /// <summary>
        /// Me va a permitir obtener los HECTOLITROS que le corresponderán a las
        /// PULGADAS CUBICAS que queremos obtener en la entrada por el usuario.
        /// </summary>
        /// <param name="plg">Las PULGADAS CUBICAS que deseo convertir
        /// a HECTOLITRO</param>
        /// <returns>Nos devolverá los HECTOLITROS que le corresponden
        /// en PULGADAS CUBICAS ingresados por el usuario.</returns>
        public decimal convPlgCubAHect(double plg)
        {
            return decimal.Parse(plg / 6102).ToString());
        }
    }
}
