﻿using System;
using ConversionDeUnidades;

namespace ConversionDeUnidades_PruebaExe
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //AAA
            //arrange
            var longitud = new Longitud();
            //act
            decimal res = longitud.convMmAPlg(1032);
            //assert
            if (res == 40.62992125984252m)
            {
                Console.WriteLine("Se es el mismo..." + res);
            }
            else
            {
                Console.WriteLine("FALLÓ, no es el mismo...res="+res+" y se esperaba="+ 40.62992125984252m);
            }//AAA
            //arrange
             longitud = new Longitud();
            //act
             res = longitud.convCmAPlg(1031);
            //assert
            if (res == 40.62992125984252m)
            {
                Console.WriteLine("Se es el mismo..." + res);
            }
            else
            {
                Console.WriteLine("FALLÓ, no es el mismo...res="+res+" y se esperaba="+ 40.62992125984252m);
            }

        }
    }
}
