﻿using NUnit.Framework;
using System;
using ConversionDeUnidades;
namespace ConversionDeUnidades.UnitTest
{
    [TestFixture()]
    public class TestLongitud
    {
        [Test()]
        public void Test_ConvertirMmAPlg_ResultaDecimal()
        {
            ///AAA
            //arrange
            var longitud = new Longitud();
            //act
            //
            decimal res = longitud.convMmAPlg(1032);
            //assert
            Assert.AreEqual(40.629921259842519685039370079m, res);
        }

        [Test()]
        public void Test_ConvertirCmAPlg_ResultaDecimal()
        {
            ///AAA
            //arrange
            var longitud = new Longitud();
            //act
            decimal res= longitud.convCmAPlg(1031);
            //assert
            Assert.AreEqual(405.90551181102362204724409449m, res);
        }
    }
}
